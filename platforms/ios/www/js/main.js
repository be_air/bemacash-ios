$(document).on('ready', function(e) {

    $('.btn-entrar').click(function(){
        troca($(this));
    });

    $('.btn-texto').click(function(){
        zoominn($(this));
    });
    $('.link').click(function(){
        zoominn($(this));
    });
    $('.tela').click(function(){
        zoominn($(this));
    });
    $('.prev').click(function(){
        troca($(this));
    });
    $('.next').click(function(){
        troca($(this));
    });
    $('.btn-reborn').click(function(){
    troca($(this));
    });
    $('.img-interna').click(function(){
        zoomout();
    });
     $('.btn-voltar').click(function(){
        zoomout();
    });
    
    function zoominn(botao){
        
        var zoom = botao.data("zoom");
        var step = botao.data("step");
        var zoomIn = $(".img-interna");
        var link = $(".link");
        var tela = $(".tela1");
        var voltar = $(".btn-voltar");
        var controle = $('.btn-controle');
        
        if ( zoom ){
            botao.data('zoom', false);
            botao.attr('data-zoom', false);
            link.data('zoom', false);
            link.attr('data-zoom', false);
            zoomIn.addClass('zoom-in');
            setTimeout(function(){
                link.addClass('zoom-link');
            }, 400);
            tela.delay(200).show(0); 
            voltar.delay(200).show(0);
            
            
            $('.btn-texto').hide();
            $('.mouse').hide();
            controle.hide();
        } else {
            $(".internas").fadeOut(500);
            $('#'+step).delay(500).fadeIn(500);
            voltar.fadeOut(500);
            
            zoomout();
            if(step == 'page-6'){
                $("#vid1").get('0').play();
                video1();
            }
            else if(step == 'page-8'){
                $("#vid2").get('0').play();
                video2();
            }
        }
    };
    
    function zoomout(){
        
        var botao = $('.btn-texto');
        var zoomIn = $(".img-interna");
        var link = $(".link");
        var linkImg = $(".img-interna");
        var tela = $(".tela1");
        var voltar = $(".btn-voltar");
        var controle = $('.btn-controle');
        
        botao.data('zoom', true);
        botao.attr('data-zoom', true);
        link.data('zoom', true);
        link.attr('data-zoom', true);
        zoomIn.addClass('zoom-in');
        $('.btn-texto').show();
        $('.mouse').show();
        tela.fadeOut(500);
        link.show(); 
        link.removeClass('zoom-link');
        linkImg.removeClass('zoom-in');
        voltar.fadeOut(500);
        controle.show();
        
    };
    
    function troca(btn){
                console.log(btn);

        var step = btn.data('step');
        console.log(step);
        $(".internas").fadeOut(500);
        $('#'+step).delay(500).fadeIn(500); 
        if(step == 'page-6'){
            $("#vid1").get('0').play();
            video1();
        }
        else if(step == 'page-8'){
            $("#vid2").get('0').play();
            video2();
        }
    };
    
    function video1(){
        $(".internas").delay(2700).fadeOut(500);
        $('#page-7').delay(1000).fadeIn(500);
        

    };
    function video2(){
        $(".internas").delay(1700).fadeOut(500);
        $('#page-9').delay(1000).fadeIn(500);
    };
});